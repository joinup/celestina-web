// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
// API_ROOT: 'http://localhost:8000/api',
// url: 'http://localhost:8000'
// API_ROOT: 'http://www.misempleados.com/celestina/api/public/index.php/api',
// url: 'http://www.misempleados.com/celestina/api/public'

export const environment = {
  	production: true,
	API_ROOT: 'http://www.fundagov.com/celestina/api/public/index.php/api',
  url: 'http://www.fundagov.com/celestina/api/public'
};

/*export const environment = {
  production: false,
  API_ROOT: 'http://www.fundagov.com/celestina/api/public/index.php/api',
  url: 'http://www.fundagov.com/celestina/api/public'
};*/

