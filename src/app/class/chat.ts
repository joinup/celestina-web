export class Chat {
	id?: number;
	date: number;
	read? = false;
	text: string;
	user_receiver: number;
	user_send: number;
	created_at: string;
}