export class Date {
	id: number;
	user: number;
	name: string;
	avatar: string;
	date: string;
	duration: string;
	location: string;
	price: number;
	time: string;
	type: string;
	with_user: number;
	favorite = false;
	user_favorites: Array<any>;
}