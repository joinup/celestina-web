export class User {
	id: number;
	name: string;
	email: string;
	nickname: string;
	phone: string;
	bio: string;
	birthdate: string;
	gender: string;
	activated: number;
	interest: any;
	level_education: string;
	tdc: boolean;
	paypal: boolean;
	registration_step: number;
	avatar: string;
	password: string;
}