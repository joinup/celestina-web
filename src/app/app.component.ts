import { Component } from '@angular/core';
import { ProfileService } from './services/profile/profile.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app works!';

	constructor(public profileService: ProfileService) { }

	ngOnInit() {
	  	// this.profileService.get().toPromise().then(
	   //      response => {
	   //          localStorage.setItem('profile', JSON.stringify(response.json()));
	   //      },
	   //      error => {
	   //      	console.log(error);
	   //      }
	   //  );
	}
}
