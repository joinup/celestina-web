import { NgModule } from '@angular/core';
import { Routes, RouterModule, CanActivate } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

// componet
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ConfigComponent } from './components/config/config.component';
import { AvatarComponent } from './components/avatar/avatar.component';
import { NewDateComponent } from './components/dates/new-date/new-date.component';
import { DatesComponent } from './components/dates/dates.component';
import { PerfilComponent } from './components/perfil/perfil.component';
import { RecoverPasswordComponent } from './components/recover-password/recover-password.component';
import { NovalidateComponent } from './components/novalidate/novalidate.component';
import { MyDatesComponent } from './components/dates/my-dates/my-dates.component';
import { ChatComponent } from './components/chat/chat.component';
import { ConfirmedComponent } from './components/confirmed/confirmed.component';
import { RateComponent } from './components/rate/rate.component';
import { ChatsComponent } from './components/chats/chats.component';
import { DialogChangePriceComponent } from './components/chat/dialog-change-price/dialog-change-price.component';


// services
import { LoginService } from './services/login/login.service';
import { ProfileService } from './services/profile/profile.service';

const routes: Routes = [
	{ path: 'login', component: LoginComponent },
	{ path: 'register', component: RegisterComponent },
	{ path: 'recover_password', component: RecoverPasswordComponent },
	{ path: 'chat/:date/:user', component: ChatComponent , canActivate: [ProfileService]},
	{ 
		path: '',
		component: SidebarComponent,
		canActivate: [LoginService],
		children: [
			{ path: '', redirectTo: 'dashboard', pathMatch: 'full' },
			{ path: 'dashboard', component: DashboardComponent, canActivate: [ProfileService]  },
			{ path: 'config', component: ConfigComponent },
			{ path: 'avatar', component: AvatarComponent },
			{ path: 'date', component: DatesComponent, canActivate: [ProfileService]},
			{ path: 'new_date', component: NewDateComponent, canActivate: [ProfileService]},
			{ path: 'my_date', component: MyDatesComponent, canActivate: [ProfileService]},
			{ path: 'perfile', component: PerfilComponent },
			{ path: 'novalidate', component: NovalidateComponent },
			{ path: 'confirmed/:date', component: ConfirmedComponent, canActivate: [ProfileService] },
			{ path: 'rate/:date', component: RateComponent, canActivate: [ProfileService] },
			{ path: 'chat', component: ChatsComponent, canActivate: [ProfileService] },
			{ path: 'price/:date', component: DialogChangePriceComponent, canActivate: [ProfileService] },
		]
	},
];

// @NgModule({
//   imports: [RouterModule.forChild(routes)],
//   exports: [RouterModule]
// })
// export class RoutingModule { }

export const RoutingModule: ModuleWithProviders = RouterModule.forRoot(routes, { useHash: true });


