import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpModule, Http, RequestOptions } from '@angular/http';

import { APP_BASE_HREF } from '@angular/common';

import { AuthHttp, AuthConfig  } from 'angular2-jwt';
export function authHttpServiceFactory(http: Http, options: RequestOptions) {
  return new AuthHttp(new AuthConfig({
    tokenName: 'id_token',
    tokenGetter: (() => localStorage.getItem('id_token')),
    globalHeaders: [{'Content-Type':'application/json'}],
  }), http, options);
}

import { Angular2PromiseButtonModule } from 'angular2-promise-buttons/dist';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MdButtonModule,
  MdCheckboxModule,
  MdInputModule,
  MdIconModule,
  MdToolbarModule,
  MdSidenavModule,
  MdDatepickerModule,
  MdNativeDateModule,
  MdSelectModule,
  DateAdapter,
  MdSnackBarModule,
  MdProgressSpinnerModule,
  MdGridListModule,
  MdChipsModule,
  MdSliderModule,
  MdDialogModule,
  MdCardModule,
  MdListModule
  

} from '@angular/material';
import { ImageCropperModule } from 'ng2-img-cropper';
import { AgmCoreModule } from '@agm/core';
import { Md2Module }  from 'md2';
import { MomentModule } from 'angular2-moment';
import { StarRatingModule } from 'angular-star-rating';
import { RatingModule } from "ngx-rating";
import { FacebookModule } from 'ng2-facebook-sdk';
import { CurrencyMaskModule,  } from "ng2-currency-mask";
import { CurrencyMaskConfig, CURRENCY_MASK_CONFIG } from "ng2-currency-mask/src/currency-mask.config";
 
export const CustomCurrencyMaskConfig: CurrencyMaskConfig = {
    align: "left",
    allowNegative: true,
    allowZero: true,
    decimal: ".",
    precision: 0,
    prefix: "",
    suffix: "",
    thousands: ","
};

import { RoutingModule } from './routing.module'

import { LoginService } from './services/login/login.service';
import { RegisterService } from './services/register/register.service';
import { ProfileService } from './services/profile/profile.service';
import { InterestService } from './services/interest/interest.service';
import { DateService } from './services/date/date.service';
import { ChatService } from './services/chat/chat.service';
import { UploadFilesService } from './services/upload.service';

import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';

import { RegisterComponent } from './components/register/register.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ConfigComponent } from './components/config/config.component';
import { AvatarComponent } from './components/avatar/avatar.component';
import { NewDateComponent } from './components/dates/new-date/new-date.component';
import { DialogComponent } from './components/dates/dialog/dialog.component';
import { InterestSelectComponent, MyFilterPipe } from './components/interest-select/interest-select.component';
import { DatesComponent } from './components/dates/dates.component';
import { PerfilComponent } from './components/perfil/perfil.component';
import { RecoverPasswordComponent } from './components/recover-password/recover-password.component';
import { NovalidateComponent } from './components/novalidate/novalidate.component';
import { MyDatesComponent } from './components/dates/my-dates/my-dates.component';
import { CardComponent } from './components/dates/card/card.component';
import { DialogFavoritesComponent } from './components/dates/dialog-favorites/dialog-favorites.component';
import { ChatComponent } from './components/chat/chat.component';
import { ConfirmedComponent } from './components/confirmed/confirmed.component';
import { RateComponent } from './components/rate/rate.component';
import { StarRatingComponent } from './components/star-rating/star-rating.component';
import { ChatsComponent } from './components/chats/chats.component';
import { DialogChangePriceComponent } from './components/chat/dialog-change-price/dialog-change-price.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SidebarComponent,
    RegisterComponent,
    DashboardComponent,
    ConfigComponent,
    AvatarComponent,
    NewDateComponent,
    DialogComponent,
    InterestSelectComponent,
    MyFilterPipe,
    DatesComponent,
    PerfilComponent,
    RecoverPasswordComponent,
    NovalidateComponent,
    MyDatesComponent,
    CardComponent,
    DialogFavoritesComponent,
    ChatComponent,
    ConfirmedComponent,
    RateComponent,
    StarRatingComponent,
    ChatsComponent,
    DialogChangePriceComponent,
  ],
  imports: [
    RoutingModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    BrowserAnimationsModule,
    CurrencyMaskModule,
    StarRatingModule.forRoot(),
    RatingModule,
    Angular2PromiseButtonModule.forRoot({
        //spinnerTpl: '<md-spinner style="width: 30px; height: 30px; margin: 0; float: left;"></md-spinner>',
        disableBtn: true,
        btnLoadingClass: 'is-loading',
        handleCurrentBtnOnly: false,
    }),
    ImageCropperModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAUSuL0YXReHLBQm4LwBONSuN7XbsQxlV4',
      libraries: ["places"]
    }),
    FacebookModule.forRoot(),
    Md2Module,
    MomentModule,
    MdButtonModule,
    MdCheckboxModule,
    MdInputModule,
    MdIconModule,
    MdToolbarModule,
    MdSidenavModule,
    MdDatepickerModule,
    MdNativeDateModule,
    MdSelectModule,
    MdSnackBarModule,
    MdProgressSpinnerModule,
    MdGridListModule,
    MdChipsModule,
    MdSliderModule,
    MdDialogModule,
    MdCardModule,
    MdListModule,

  ],
  providers: [
    LoginService,
    RegisterService,
    ProfileService,
    InterestService,
    UploadFilesService,
    DateService,
    ChatService,
    {
      provide: AuthHttp,
      useFactory: authHttpServiceFactory,
      deps: [ Http, RequestOptions ]
    },
    { provide: APP_BASE_HREF, useValue: '/' },
    { provide: CURRENCY_MASK_CONFIG, useValue: CustomCurrencyMaskConfig },

  ],
  entryComponents: [DialogComponent, DialogFavoritesComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
