import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { WebService } from '../web.service';
@Injectable()
export class InterestService extends WebService{

	get(): Observable<Response> {
        return this.http.get( `${this.apiUrl}/interest`, { headers: this.headers } );
    }

    add(body: Object): Observable<Response> {
        return this.http.post( `${this.apiUrl}/interest`, JSON.stringify(body), { headers: this.headers } );
    }

    update(body: Object): Observable<Response> {
        return this.http.put( `${this.apiUrl}/interest`, JSON.stringify(body), { headers: this.headers } );
    }

    delete(): Observable<Response> {
        return this.http.delete( `${this.apiUrl}/interest`, { headers: this.headers } );
    }

}
