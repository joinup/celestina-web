import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
import { tokenNotExpired } from 'angular2-jwt';

@Injectable()
export class UploadFilesService {

    public apiUrl = environment.API_ROOT;
    public headers: Headers = new Headers();
    public options: RequestOptions;
    public progress$: Observable<number>;
    public progress: number;
    public progressObserver: any;
    
    constructor(public http: Http, public router: Router) {
        this.headers.append('Authorization', 'Bearer ' + localStorage.getItem('id_token'));
        this.options = new RequestOptions({ headers: this.headers });
    }

    public updateAvatar(files: File[]): Observable<Response> {
        const url = `${this.apiUrl}/profile/avatar`;
        const formData: FormData = new FormData();
        for (let i = 0; i < files.length; i++) {
            formData.append('file', files[i], files[i].name);
        }
        return this.http.post(url, formData, this.options);
    }
}
