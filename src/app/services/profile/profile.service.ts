import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { tokenNotExpired } from 'angular2-jwt';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';


import { WebService } from '../web.service';

@Injectable()
export class ProfileService extends WebService{

	canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {

        
        let profile = JSON.parse(localStorage.getItem('profile'));
        if (profile) {
            return this.verificate(profile);
        } else {
            this.get().toPromise().then(
                response => {
                    localStorage.setItem('profile', JSON.stringify(response.json()));
                    return this.verificate(response.json());
                },
                error => {
                    return false;
                }
            )
        }
        return true;

    }

    verificate(profile){
        
        if ( profile.registration_step == 1 || profile.registration_step == "1" ) {
            this.router.navigate(['/avatar']);
            return false;
        }
        if ( profile.registration_step == 2 || profile.registration_step == "2" ) {
            this.router.navigate(['/config']);
            return false;
        }
        if ( profile.activated == 1  || profile.activated == "1" ) {
            this.router.navigate(['/novalidate']);
            return false;
        }

        return true;
    
    }


	get(): Observable<Response> {
		this.refreshToken();
        return this.http.get( `${this.apiUrl}/profile`, { headers: this.headers } );
    }

    update(body: Object): Observable<Response> {
        return this.http.put( `${this.apiUrl}/profile`, JSON.stringify(body), { headers: this.headers } );
    }


}
