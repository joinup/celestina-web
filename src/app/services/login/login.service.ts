import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { tokenNotExpired } from 'angular2-jwt';

import { WebService } from '../web.service';

@Injectable()
export class LoginService extends WebService {

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
        if ( this.loggedIn() ) {
            return true;
        }
        if ( state.url === '/login' && this.loggedIn() ) {
            this.router.navigate(['/']);
            return false;
        }
        this.router.navigate(['/login']);
        return false;
    }

    login(body: Object): Observable<Response> {
        this.refreshToken();
        return this.http.post( `${this.apiUrl}/login`, JSON.stringify(body), { headers: this.headers } );
    }

    loginFacebook(body: Object): Observable<Response> {
        this.refreshToken();
        return this.http.post( `${this.apiUrl}/auth/facebook`, JSON.stringify(body), { headers: this.headers } );
    }

    passwordRecovery(body: Object): Observable<Response> {
        return this.http.post(`${this.apiUrl}/password_recovery`, body);
    }
}
