import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
import { AuthHttp, tokenNotExpired } from 'angular2-jwt';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class WebService {

    public apiUrl = environment.API_ROOT;
    public headers: Headers;
    public options: RequestOptions;
    constructor(public http: Http, public router: Router, public authHttp: AuthHttp) {
        this.refreshToken();
    }
    public loggedIn() {
      return tokenNotExpired('id_token');
      //return localStorage.getItem('id_token');
    }
    public refreshToken() {
        this.headers = new Headers({ 'Content-Type': 'application/json' });
        if (this.loggedIn()) {
            this.headers.append('Authorization', 'Bearer ' + localStorage.getItem('id_token'));
        }
        this.options = new RequestOptions({ headers: this.headers });
    }
    public logout() {
        localStorage.removeItem('id_token');
        localStorage.clear();
        this.router.navigateByUrl('/login');
    }
}
