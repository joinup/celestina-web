import { Injectable } from '@angular/core';
import { Response, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { WebService } from '../web.service';
@Injectable()
export class ChatService extends WebService{

	get(date: string, user: string ): Observable<Response> {
		let params = new URLSearchParams();
  			params.set('date', date);
  			params.set('user', user);
        return this.http.get(`${this.apiUrl}/chat`, { headers: this.headers, search: params } );
    }

    sent(body: Object): Observable<Response> {
        return this.http.post( `${this.apiUrl}/chat`, JSON.stringify(body), { headers: this.headers } );
    }

    read(date: string, user: string): Observable<Response> {
		let params = new URLSearchParams();
  			params.set('date', date);
  			params.set('user', user);
        return this.http.get(`${this.apiUrl}/chat/new`, { headers: this.headers, search: params } );
    }

    all(): Observable<Response> {
        return this.http.get( `${this.apiUrl}/chat/all`, { headers: this.headers } );
    }

}
