import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { WebService } from '../web.service';

@Injectable()
export class DateService extends WebService{

	get(): Observable<Response> {
        this.refreshToken();
        return this.http.get( `${this.apiUrl}/date`, { headers: this.headers } );
    }

    find(date: number): Observable<Response> {
        this.refreshToken();
        return this.http.get( `${this.apiUrl}/date/${date}`, { headers: this.headers } );
    }

    add(body: Object): Observable<Response> {
        this.refreshToken();
        return this.http.post( `${this.apiUrl}/date`, JSON.stringify(body), { headers: this.headers } );
    }

    types(): Observable<Response> {
        this.refreshToken();
        return this.http.get( `${this.apiUrl}/date/types`, { headers: this.headers } );
    }

    addFavorite(date: number): Observable<Response>{
        this.refreshToken();
        return this.http.post( `${this.apiUrl}/date/${date}/favorite`, '', { headers: this.headers } );
    }

    removeFavorite(date: number): Observable<Response>{
        this.refreshToken();
        return this.http.delete( `${this.apiUrl}/date/${date}/favorite`, { headers: this.headers } );
    }

    myDates(): Observable<Response> {
        this.refreshToken();
        return this.http.get( `${this.apiUrl}/date/my`, { headers: this.headers } );
    }

    myFavorites(): Observable<Response> {
        this.refreshToken();
        return this.http.get( `${this.apiUrl}/date/favorites`, { headers: this.headers } );
    }

    myConfirmed(): Observable<Response> {
        this.refreshToken();
        return this.http.get( `${this.apiUrl}/date/confirmed`, { headers: this.headers } );
    } 

    accept(body: Object): Observable<Response> {
        this.refreshToken();
        return this.http.post( `${this.apiUrl}/date/accept`, JSON.stringify(body), { headers: this.headers } );
    }

    rate(date: number): Observable<Response> {
        this.refreshToken();
        return this.http.get( `${this.apiUrl}/date/${date}/rate`, { headers: this.headers } );
    } 

    addRate(date: number, body: Object): Observable<Response> {
        this.refreshToken();
        return this.http.post( `${this.apiUrl}/date/${date}/rate`, JSON.stringify(body), { headers: this.headers } );
    }

    editPrice(date: number, body: Object): Observable<Response> {
        this.refreshToken();
        return this.http.post( `${this.apiUrl}/date/${date}/price`, JSON.stringify(body), { headers: this.headers } );
    }
}
