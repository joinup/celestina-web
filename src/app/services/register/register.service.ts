import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { tokenNotExpired } from 'angular2-jwt';

import { WebService } from '../web.service';
@Injectable()
export class RegisterService extends WebService{

	register(body: Object): Observable<Response> {
        return this.http.post( `${this.apiUrl}/signup`, JSON.stringify(body), { headers: this.headers } );
    }
    
    registerFacebook(body: Object): Observable<Response> {
        return this.http.post( `${this.apiUrl}/signup/facebook`, JSON.stringify(body), { headers: this.headers } );
    }

}
