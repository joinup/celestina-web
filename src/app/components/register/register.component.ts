import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, AbstractControl, FormBuilder, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { MdDatepicker, MdSnackBar } from '@angular/material';
import { FacebookService, InitParams, LoginResponse } from 'ng2-facebook-sdk';
import * as moment from 'moment';

import { RegisterService } from '../../services/register/register.service';
import { User } from 'app/class/user';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

    public form: FormGroup;
    public name: AbstractControl;
    public email: AbstractControl;
    public phone: AbstractControl;
    public nickname: AbstractControl;
    public gender: AbstractControl;
    public birthdate: AbstractControl;
    public level_education: AbstractControl;
    public password: AbstractControl;
    public repeatPassword: AbstractControl;
    public promiseLoading: Promise<any>;

    public maxDate = new Date();

    @ViewChild(MdDatepicker) datepicker: MdDatepicker<Date>;

      constructor(
          formB: FormBuilder,
          private fb: FacebookService,
          private registerService: RegisterService,
          private snackBar: MdSnackBar,
          private router: Router,
        ) {
        this.maxDate.setFullYear( this.maxDate.getFullYear() - 18 );

          let pattern = "[0-9\-\+\s\(\)]*$";
        this.form = formB.group({
            'email': ['', Validators.compose([Validators.required, Validators.minLength(4), Validators.email])],
            'name' : ['',Validators.compose([Validators.required])],
            'phone' : ['', Validators.compose([Validators.pattern(pattern)])],
            'nickname' : ['',Validators.compose([Validators.required])],
            'gender' : [''],
            'birthdate' : [null, Validators.required],
            'level_education' : [''],
            'password': ['', Validators.compose([Validators.required, Validators.minLength(4)])],
            'repeatPassword' : ['',Validators.compose([Validators.required, Validators.minLength(4)])],
        });

        this.email = this.form.controls['email'];
        this.password = this.form.controls['password'];
        this.name = this.form.controls['name'];
        this.email = this.form.controls['email'];
        this.phone = this.form.controls['phone'];
        this.nickname = this.form.controls['nickname'];
        this.gender = this.form.controls['gender'];
        this.birthdate = this.form.controls['birthdate'];
        this.level_education = this.form.controls['level_education'];
        this.password = this.form.controls['password'];
        this.repeatPassword = this.form.controls['repeatPassword'];

        let initParams: InitParams = {
          appId: '731528770363855',
          cookie: true,
          xfbml: true,
          version: 'v2.8'
        };
        
        this.fb.init(initParams);

    }


    ngOnInit() {}

    public onSubmit(values: User) {
        if (this.form.valid) {
            if (values.birthdate !== "") {
                values.birthdate = moment(values.birthdate).format('Y-MM-DD')
            }
            this.promiseLoading = this.registerService.register(values).toPromise().then(
                response => {
                    this.openSnackBar('Registrado con exito');
                    setTimeout( () => {
                        this.router.navigate(['/login']);
                    },1000);
                },
                error => {
                    if ( error.status === 402 ) {
                        console.log(error)
                      this.openSnackBar(error.json().error);
                    } else {
                        this.openSnackBar('¡Ha ocurrido un error! Intente más tarde');
                    }
                }
            );
        } else {
            this.openSnackBar('Complete los campos');
        }
    }

    public registerWithFacebook() {
        this.fb.login()
            .then((response: LoginResponse) => {
                console.log(response)
                if (response.status === 'connected') {
                    this.registerService.registerFacebook({code: response.authResponse.accessToken}).toPromise().then(
                        response => {
                            this.openSnackBar('Registrado con exito');
                           
                            this.router.navigate(['/login']);
                        },
                        error => {
                            if ( error.status === 401 ) {
                                this.openSnackBar(error.json().error);
                            }
                            if ( error.status === 500 ) {
                                this.openSnackBar('¡Ha ocurrido un error! Intente más tarde');
                            }
                        },
                    );
                }
            })
            .catch((error: any) => console.error(error));
    }

    public openSnackBar(msg) {
        this.snackBar.open(msg,'', {
          duration: 2000,
        });
    }

}