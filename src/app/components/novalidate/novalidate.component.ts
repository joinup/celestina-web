import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProfileService } from '../../services/profile/profile.service';
@Component({
  selector: 'app-novalidate',
  templateUrl: './novalidate.component.html',
  styleUrls: ['./novalidate.component.scss']
})
export class NovalidateComponent implements OnInit {

	constructor(public profileService: ProfileService, public router: Router) { }

	ngOnInit() {
		this.getProfile();
	}

	public getProfile() {
        this.profileService.get().toPromise().then(
            response => {
                localStorage.setItem('profile', JSON.stringify(response.json()));
                if (+response.json().activated == 2) {
                	console.log(+response.json().activated);
 					this.router.navigate(['/']);
                } 
		    },
            error => {
                console.log(error);
            }
        );
    }

}
