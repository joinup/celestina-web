import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NovalidateComponent } from './novalidate.component';

describe('NovalidateComponent', () => {
  let component: NovalidateComponent;
  let fixture: ComponentFixture<NovalidateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NovalidateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NovalidateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
