import { Component, OnInit, ViewChild } from '@angular/core';
import { ImageCropperComponent, CropperSettings } from 'ng2-img-cropper';
import { Router } from '@angular/router';
import { MdSnackBar } from '@angular/material';

import { UploadFilesService } from '../../services/upload.service';
import { ProfileService } from '../../services/profile/profile.service';
import { environment } from 'environments/environment';
import { User } from '../../class/user';

import { SidebarComponent } from '../sidebar/sidebar.component';

@Component({
  selector: 'app-avatar',
  templateUrl: './avatar.component.html',
  styleUrls: ['./avatar.component.scss']
})
export class AvatarComponent implements OnInit {

	public url = environment.url;
	public uploadedAvatar: any[] = [];
	public user: User; 
	public avatar: any;
	public prewiewAvatar: any;
	public defaultAvatar: any;
	public display: boolean = false;
	public loading: boolean = false;
	public promiseLoading: Promise<any>;
	data: any;
    cropperSettings: CropperSettings;



    @ViewChild('cropper', undefined)
	cropper: ImageCropperComponent;

	constructor(
		public uploadFilesService: UploadFilesService,
		public profileService: ProfileService,
		private router: Router,
		private snackBar: MdSnackBar,

		private sidebar: SidebarComponent,
		 ) {

		this.cropperSettings = new CropperSettings();
		this.cropperSettings.noFileInput = true;
        this.cropperSettings.width = 300;
        this.cropperSettings.height = 300;
        this.cropperSettings.croppedWidth =300;
        this.cropperSettings.croppedHeight = 300;
        // this.cropperSettings.canvasWidth = 400;
        // this.cropperSettings.canvasHeight = 300;

        this.data = {};

	}

	ngOnInit() {
		this.defaultAvatar = './assets/add_img.png';

		this.prewiewAvatar = this.defaultAvatar;
		this.getProfile();
	}

	public getProfile() {
        this.profileService.get().toPromise().then(
            response => {
                let profile = response.json();
                if (profile.avatar) {
					this.prewiewAvatar = this.url + profile.avatar;                	
                }
                localStorage.setItem('profile', JSON.stringify(response.json()));
            },
            error => {
                console.log(error);
            }
        );
    }

	fileChangeListener(event) {
		this.uploadedAvatar = [];
	    var image:any = new Image();
	    var file:File = event.target.files[0];
	    var reader:FileReader = new FileReader();
	    var that = this;
	    reader.onloadend = function (loadEvent:any) {
	        image.src = loadEvent.target.result;
	        that.cropper.setImage(image);
	    };
	    reader.readAsDataURL(file);
		this.display = true;
	}

	public save(){
		this.display = false;
		this.updateAvatar();
	}

	public close() {
		this.display = false;
	}

    public updateAvatar() {
    	this.loading = true;
    	let file = this.dataURItoBlob(this.data.image);

	    this.uploadedAvatar.push(file);
        this.promiseLoading = this.uploadFilesService.updateAvatar(this.uploadedAvatar).toPromise().then(
            response => {
    			this.loading = false;
				this.prewiewAvatar = this.data.image;
				localStorage.setItem('profile', JSON.stringify(response.json()));
				this.openSnackBar('Imagen actualizada');
				this.sidebar.getProfile();
				if (response.json().registration_step == 2) {
					setTimeout(() =>{
                    	this.router.navigate(['/config']);
                	},1500);
                }
            },
            error => {
				this.openSnackBar('¡Ha ocurrido un error! Intente más tarde');
    			this.loading = false;
            }
         );
    }

    public openSnackBar(msg) {
        this.snackBar.open(msg,'', {
          duration: 2000,
        });
    }

    private dataURItoBlob(dataURI) {
	    var binary = atob(dataURI.split(',')[1]);
	    var array = [];
	    for(var i = 0; i < binary.length; i++) {
	        array.push(binary.charCodeAt(i));
	    }
	    return new Blob([new Uint8Array(array)], {type: 'image/jpeg'});
	}

}
