import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, AbstractControl, FormBuilder, Validators} from '@angular/forms';
import * as moment from 'moment';
import { MdSnackBar } from '@angular/material';
import { ProfileService } from '../../services/profile/profile.service';
import { DateService } from '../../services/date/date.service';
import { environment } from 'environments/environment';
import { User } from '../../class/user';
import { Date } from '../../class/date';

@Component({
  selector: 'app-rate',
  templateUrl: './rate.component.html',
  styleUrls: ['./rate.component.scss']
})
export class RateComponent implements OnInit {

	public url = environment.url;
	public prewiewAvatar = './assets/avatar.png';
	public id_date: number;
	public profile: User;
	public user: User;
	public date: Date;
	public form: FormGroup;
    public text: AbstractControl;
    public stars = 0;

    public promiseLoading: Promise<any>;

    public disabled = false;

  	constructor(
  		fb: FormBuilder,
  		public profileService: ProfileService,
  		public dateService: DateService,
  		private route: ActivatedRoute,
  		private router: Router,
  		private snackBar: MdSnackBar,

  	) { 
		this.form = fb.group({
          'text': ['', Validators.compose([Validators.required])],
        });

        this.text = this.form.controls['text'];
  	}

	ngOnInit() {


		this.route.params.subscribe(params => {
           this.id_date = params['date'];
        });
		this.getProfile();
	}

	public getProfile() {
	    this.profileService.get().toPromise().then(
	        response => {
	            this.profile = response.json();
	            localStorage.setItem('profile', JSON.stringify(response.json()));
	            this.getDate(this.id_date);
				this.getRate(this.id_date);
	        },
	        error => {
	            console.log(error);
	        }
	    );
	}

	public getDate(date: number) {
	    this.dateService.find(date).toPromise().then(
	        response => {
	            this.date = response.json();
	           	if (response.json().user_info.id === this.profile.id ) {
	           		this.user = response.json().with_user;
	           	} else if (response.json().with_user.id === this.profile.id ) {
	           		this.user = response.json().user_info;
	           	} else {
	           		this.router.navigate(['/']);
	           	}
	        },
	        error => {
	            console.log(error);
	        }
	    );
	}
	public getRate(date: number) {
	    this.dateService.rate(date).toPromise().then(
	        response => {
	            if (response.json().stars){
	            	this.stars = response.json().stars;
	            	this.text.setValue(response.json().text);
	            	this.disabled = true;
	            }
	        },
	        error => {
	            console.log(error);
	        }
	    );
	}
	public getAvatar(user: User): string{
        if (user) {
            return this.url + user.avatar;
        } else {
            return this.prewiewAvatar;
        }
    }

    public changeStar(event: number) {
    	console.log(event);
    	this.stars = event
    }

    public onSubmit(values) {
    	if (this.form.valid) {
    		values.stars = this.stars;
    		values.user  = this.user.id;

    		this.promiseLoading = this.dateService.addRate(this.date.id, values).toPromise().then(
    			response => {
    				this.openSnackBar('Calificación guardada con exito')
    			},
    			error => {
    				if ( error.status === 400 ) {
                      this.openSnackBar(error.json().error);
                    } else {
                    	
                      this.openSnackBar('¡Ha ocurrido un error! Intente más tarde');
                    }
    			}
    		)
    	}
    }

    public openSnackBar(msg) {
        this.snackBar.open(msg,'', {
            duration: 2000,
        });
    }

}
