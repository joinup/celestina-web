import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InterestSelectComponent } from './interest-select.component';

describe('InterestSelectComponent', () => {
  let component: InterestSelectComponent;
  let fixture: ComponentFixture<InterestSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InterestSelectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InterestSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
