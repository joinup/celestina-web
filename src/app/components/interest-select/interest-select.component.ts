import { Component, OnInit, OnChanges, Input, Output, EventEmitter } from '@angular/core';



import { InterestService } from '../../services/interest/interest.service';

import { Interest } from '../../class/interest';


@Component({
  selector: 'app-interest-select',
  templateUrl: './interest-select.component.html',
  styleUrls: ['./interest-select.component.scss']
})
export class InterestSelectComponent implements OnChanges {

	@Output() interest: EventEmitter<object> = new EventEmitter<object>();
	@Input() oldInterests: Array<any>;
    public interests: Interest[];
    public selectInterests: Array<number>;
    public displayInterests: Interest[];
    public _search: string;

	constructor(public interestService: InterestService,) {
		this.selectInterests = [];
		this.getInterests();
	}

	ngOnChanges() {
        this.getInterests();
    }

      public getInterests() {
        this.interestService.get().toPromise().then(
            response => {
                this.interests = response.json();
                if (this.oldInterests && !this.selectInterests.length) {
                    for (var i = this.oldInterests.length - 1; i >= 0; i--) {
                        this.selectInterests.push(this.oldInterests[i]);
                    }
                }
                this.setDisplayInterest();
            },
            error => {
                console.log(error);

            }

        );
    }

    public change() {
        this.setDisplayInterest();
    	this.interest.emit( { interest: this.selectInterests  } );
    }

    public setDisplayInterest() {
        this.displayInterests = [];
        this.interests.forEach((interest, i)=>{
            this.selectInterests.forEach((value, j)=>{
                if (interest.id == value) {
                    this.displayInterests.push(interest)
                }
            })
        })

    }

    // public addInterest(interest: Interest){
    //     if (this.selectInterests.indexOf(interest) === -1) {
    //         this.selectInterests.push(interest);
    //     }
    // }

    // public deleteInterest(interest: Interest){
    //     let index = this.selectInterests.indexOf(interest);
    //     if (index !== -1) {
    //         this.selectInterests.splice(index, 1);
    //     }
    // }    

    // public filterInterest(name) {
    //     this.displayInterests = [];
    //     let end = (this.interests.length > 9)? 9: this.interests.length;
    //     for (let i = this.interests.length - 1; i >= 0; i--) {
    //         let value = this.interests[i].name;
    //         if (!name || value.toLowerCase().indexOf(name.toLowerCase()) > -1 ) {
    //             this.displayInterests.push(this.interests[i]);
    //         }
    //         if (this.displayInterests.length == 9) {
    //             i = -1;
    //         }
    //     }
    // }

}

import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'myfilter',
    pure: false
})
export class MyFilterPipe implements PipeTransform {
    transform(items: Interest[], filter: string): any {
        if (!items || !filter) {
            return items;
        }
        // filter items array, items which match and return true will be kept, false will be filtered out
        return items.filter(item => item.name.indexOf(filter) !== -1);
    }
}