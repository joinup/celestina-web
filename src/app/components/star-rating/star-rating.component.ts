import { Component, OnInit, OnChanges, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-star-rating',
  templateUrl: './star-rating.component.html',
  styleUrls: ['./star-rating.component.scss']
})
export class StarRatingComponent implements OnChanges {

	@Input() stars: number = 0;
	@Input() disabled = true;
	@Output() event: EventEmitter<object> = new EventEmitter<object>();
	public totals: Array<any>;

	constructor() {
		this.totals = [
			{num: 1},
			{num: 2},
			{num: 3},
			{num: 4},
			{num: 5},
		];
	}

	ngOnChanges() {
		console.log(this.stars);
		console.log(this.disabled);
	}
	public hover(star) {
		if (!this.disabled) {
			console.log(star)
			this.stars = star.num;
			this.event.emit(star.num);
		}
	}

}
