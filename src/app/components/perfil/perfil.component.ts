import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, AbstractControl, FormBuilder, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { MdDatepicker, MdSnackBar } from '@angular/material';
import { ProfileService } from '../../services/profile/profile.service';
import { User } from 'app/class/user';
@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.scss']
})
export class PerfilComponent implements OnInit {
	
	public showPass = false;
	public user: User;
	public form: FormGroup;
    public name: AbstractControl;
    public email: AbstractControl;
    public phone: AbstractControl;
    public nickname: AbstractControl;
    public gender: AbstractControl;
    public birthdate: AbstractControl;
    public level_education: AbstractControl;
    public password: AbstractControl;
    public repeatPassword: AbstractControl;
    public promiseLoading: Promise<any>;

	@ViewChild(MdDatepicker) datepicker: MdDatepicker<Date>;

      constructor(
          fb: FormBuilder,
          private profileService: ProfileService,
          private snackBar: MdSnackBar,
          private router: Router,
        ) {
          let pattern = "[0-9\-\+\s\(\)]*$";
        this.form = fb.group({
            'email': [{value: '', disabled: true}, Validators.compose([Validators.required, Validators.minLength(4), Validators.email])],
            'name' : ['',Validators.compose([Validators.required])],
            'phone' : ['', Validators.compose([Validators.pattern(pattern)])],
            'nickname' : ['',Validators.compose([Validators.required])],
            'gender' : [''],
            'birthdate' : [null, Validators.required],
            'level_education' : [''],
            'password': ['', Validators.compose([ Validators.minLength(4)])],
            'repeatPassword' : ['',Validators.compose([ Validators.minLength(4)])],
        });

        this.email = this.form.controls['email'];
        this.name = this.form.controls['name'];
        this.email = this.form.controls['email'];
        this.phone = this.form.controls['phone'];
        this.nickname = this.form.controls['nickname'];
        this.gender = this.form.controls['gender'];
        this.birthdate = this.form.controls['birthdate'];
        this.level_education = this.form.controls['level_education'];
        this.password = this.form.controls['password'];
        this.repeatPassword = this.form.controls['repeatPassword'];
    }

	ngOnInit() {
		this.getProfile();
	}

  	public onSubmit(values: User) {
        if (this.form.valid) {
        	if (values.birthdate !== "") {
                values.birthdate = moment(values.birthdate).format('Y-MM-DD')
            }
            if (values.password === '') delete values.password;
            this.promiseLoading = this.profileService.update(values).toPromise().then(
                response => {
                    this.openSnackBar('Actualizado con exito');
                },
                error => {
                    this.openSnackBar('¡Ha ocurrido un error! Intente más tarde');
                }
            );
        } else {
              this.openSnackBar('Complete los campos');
        }
    }

    public getProfile() {
        this.profileService.get().toPromise().then(
            response => {
                localStorage.setItem('profile', JSON.stringify(response.json()));
                this.user = response.json();
                this.email.setValue(this.user.email);
		        this.name.setValue(this.user.name);
		        this.email.setValue(this.user.email);
		        this.phone.setValue(this.user.phone);
		        this.nickname.setValue(this.user.nickname);
		        this.gender.setValue(this.user.gender);
		        this.birthdate.setValue(this.user.birthdate);
		        this.level_education.setValue(this.user.level_education);
		    },
            error => {
                console.log(error);
            }
        );
    }

    public openSnackBar(msg) {
        this.snackBar.open(msg,'', {
          duration: 2000,
        });
    }

}
