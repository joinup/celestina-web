import { Component, OnInit } from '@angular/core';
import { FormGroup, AbstractControl, FormBuilder, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { MdSnackBar } from '@angular/material';
import { LoginService } from '../../services/login/login.service';

@Component({
  selector: 'app-recover-password',
  templateUrl: './recover-password.component.html',
  styleUrls: ['./recover-password.component.scss']
})
export class RecoverPasswordComponent implements OnInit {

  	public form: FormGroup;
    public email: AbstractControl;
    public promiseLoading: Promise<any>;

  	constructor(
  		fb: FormBuilder,
  		private loginService: LoginService,
  		private router: Router,
  		private snackBar: MdSnackBar,
  	) {

		this.form = fb.group({
          'email': ['', Validators.compose([Validators.required, Validators.minLength(4), Validators.email])],
        });

        this.email = this.form.controls['email'];
 	}
 	ngOnInit() {}

 	public onSubmit(values: Object): void {
        if (this.form.valid) {
            this.promiseLoading = this.loginService.passwordRecovery(values).toPromise().then(
                response => {
                    this.openSnackBar('Se ha enviado una nueva clave a su Email');
                },
                error => {
                    if ( error.status === 400 ) {
                      this.openSnackBar('Datos incorrectos');
                    }
                    if ( error.status === 500 ) {
                        this.openSnackBar('¡Ha ocurrido un error! Intente más tarde');
                    }
                }
            );
        } else {
              this.openSnackBar('Complete los campos');
        }
    }

    public openSnackBar(msg) {
        this.snackBar.open(msg,'', {
            duration: 2000,
        });
    }

}
