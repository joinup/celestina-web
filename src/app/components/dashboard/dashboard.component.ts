import { Component, OnInit, NgZone, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'environments/environment';
import { FormControl, FormsModule, ReactiveFormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { AgmCoreModule, MapsAPILoader } from '@agm/core';
import {} from '@types/googlemaps';
declare var google: any;

import { ProfileService } from '../../services/profile/profile.service';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
    public avatar: string = './assets/avatar.png';
    public name = "";
    public url = environment.url;
    public searchControl: FormControl;
    private zoom = 4;
    private latitude = 39.8282;
    private longitude = -98.5795;
    public city: string;
    public changeLocation = false;

    @ViewChild("search")
    public searchElementRef: ElementRef;


    constructor(
        public profileService: ProfileService,
        private router: Router,
        private mapsAPILoder: MapsAPILoader,
        private ngZone: NgZone,
    ) { }

    ngOnInit() {

        this.getProfile();

        this.searchControl = new FormControl();

        // set current position
        this.setCurrentPosition();

        this.city = "Ubicación";

        this.mapsAPILoder.load().then(() => {
            const autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
                types: ['(cities)'],
                componentRestrictions: {country: "co"}
            });
            autocomplete.addListener("place_changed", () => {
                this.ngZone.run(() => {
                    //get the place result
                    const place: google.maps.places.PlaceResult = autocomplete.getPlace();

                    console.log(place)


                    //verify result
                    if (place.geometry === undefined || place.geometry === null) {
                        return;
                    }

                    this.setCity(place.name);
                    this.changeLocation = false;
                    //set latitude, longitude and zoom
                    this.latitude = place.geometry.location.lat();
                    this.longitude = place.geometry.location.lng();
                    this.zoom = 12;
                });
            });
        });
    }

    public setCurrentPosition() {
        this.city = "Buscando ubicación...";
        if ('geolocation' in navigator) {
            navigator.geolocation.getCurrentPosition((position) => {
                this.latitude = position.coords.latitude;
                this.longitude = position.coords.longitude;
                this.zoom = 17;

                this.getNamePosition(this.latitude, this.longitude);

            },
            (error) => {
                console.log(error)
                this.city = "Error al buscar ubicación";
            }


            );
        }
    }

    public setCity(city: string) {
        console.log(city)
         this.city = city;    
         console.log(this.city)
        
    }

    public getNamePosition(lat,lng) {

        // lat = 4.7104318;
        // lng = -74.0777576;


        this.mapsAPILoder.load().then(() => {
            const geocoder = new google.maps.Geocoder();
            const latlng = new google.maps.LatLng(lat, lng);
            geocoder.geocode({latLng: latlng}, (results,status) => { 
                if (status == google.maps.GeocoderStatus.OK) {
                    for (let i=0; i<results[0].address_components.length; i++) {
                        for (let b=0;b<results[0].address_components[i].types.length;b++) {

                        //there are different types that might hold a city admin_area_lvl_1 usually does in come cases looking for sublocality type will be more appropriate
                            if (results[0].address_components[i].types[b] == "locality") {
                                //this is the object you are looking for
                                var city = results[0].address_components[i].long_name;
                            }
                        }
                    }
                }
                this.setCity(city);
               
            });
        });
        //this.city = "Ubicacióndsds";
    }


    public getProfile() {
        this.profileService.get().toPromise().then(
            response => {
                localStorage.setItem('profile', JSON.stringify(response.json()));
                if (response.json().avatar) {
                    this.avatar = this.url + response.json().avatar;
                }
                if (response.json().name) {
                    this.name = response.json().nickname;
                }
                /*if (response.json().registration_step == 1) {
                    this.router.navigate(['/avatar']);
                }
                if (response.json().registration_step == 2) {
                    this.router.navigate(['/config']);
                }*/
            },
            error => {
                console.log(error);
            }
        );
    }

}
