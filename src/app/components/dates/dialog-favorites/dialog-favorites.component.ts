import { Component, OnInit } from '@angular/core';
import { MdDialog, MdDialogRef } from '@angular/material';
import { Router } from '@angular/router';
import { environment } from 'environments/environment';

import { DateService } from '../../../services/date/date.service';

import { Date } from '../../../class/date';
@Component({
  selector: 'app-dialog-favorites',
  templateUrl: './dialog-favorites.component.html',
  styleUrls: ['./dialog-favorites.component.scss']
})
export class DialogFavoritesComponent implements OnInit {

	public users: Array<any>;
	public date: Date;
	public url = environment.url;
    public prewiewAvatar = './assets/avatar.png';


 	constructor(
 		public dialogRef: MdDialogRef<DialogFavoritesComponent>,
 		private router: Router,
 		public dateService: DateService
 	) { }

  	ngOnInit() {
  	}

  	public getAvatar(user): string{
        if (user) {
            return this.url + user.avatar;
        } else {
            return this.prewiewAvatar;
        }
    }

    public chat(user) {
        this.router.navigate(['/chat',this.date.id,user.id]);
    }

    public accept(user) {
    	if (!this.date.with_user) {
	    	let data = {
	    		date: this.date.id,
	    		user: user.id,
	    	}
	        this.dateService.accept(data).toPromise().then(
	        	response => {
	        		console.log(response)
              this.date.with_user = user.id;
	        		this.router.navigate(['/confirmed',this.date.id]);
	        	},
	        	error => {
	        		console.log(error)
	        	}
	        )
    	}
    }

}
