import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogFavoritesComponent } from './dialog-favorites.component';

describe('DialogFavoritesComponent', () => {
  let component: DialogFavoritesComponent;
  let fixture: ComponentFixture<DialogFavoritesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogFavoritesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogFavoritesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
