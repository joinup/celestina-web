import { Component, OnInit } from '@angular/core';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import { Router } from '@angular/router';
import { environment } from 'environments/environment';
import { DateService } from '../../../services/date/date.service';
import { ProfileService } from '../../../services/profile/profile.service';
import { Interest } from '../../../class/interest';
import { Date } from '../../../class/date';
import { DialogFavoritesComponent } from '../dialog-favorites/dialog-favorites.component';
@Component({
  selector: 'app-my-dates',
  templateUrl: './my-dates.component.html',
  styleUrls: ['./my-dates.component.scss','./my-dates.component.css']
})
export class MyDatesComponent implements OnInit {

    public type = 'creadas';
    public dateCreated: Array<Date>;
    public url = environment.url;
    public avatar = "./assets/avatar.png"
    public profile: any;
    public loading = true;


    constructor(
        public dateService: DateService,
        public dialog: MdDialog,
        private router: Router,
        private profileService: ProfileService,
    ) { }


    ngOnInit() {
        this.getMyDates();
        this.getProfile();
    }

    public getProfile() {
        this.profileService.get().toPromise().then(
            response => {
                this.profile = response.json();
                localStorage.setItem('profile', JSON.stringify(response.json()));
            },
            error => {
                console.log(error);
            }
        );
    }

    public getMyDates() {
        this.loading = true;
        this.dateService.myDates().toPromise().then(
            response => {
                this.loading = false;
                this.dateCreated = response.json();
            },
            error => {
    
            }
        )
    }

    public getMyFavorites() {
        this.loading = true;
        this.dateService.myFavorites().toPromise().then(
            response => {
                this.loading = false;
                this.dateCreated = response.json();
            },
            error => {
    
            }
        )
    }

    public getMyConfirmed() {
        this.loading = true;
        this.dateService.myConfirmed().toPromise().then(
            response => {
                this.loading = false;
                this.dateCreated = response.json();
            },
            error => {
    
            }
        )
    }

    public changeType(){
        if (this.type === 'creadas') {
            this.getMyDates();
        } else if(this.type === 'confirmadas'){
            this.getMyConfirmed();
        } else {
            this.getMyFavorites();
        }
    }

    public getAvatar(date: Date) {
        return date.avatar? this.url + date.avatar: this.avatar;
    }

    public view(date: Date) {
        let dialogRef = this.dialog.open(DialogFavoritesComponent);
        /*dialogRef.afterClosed().subscribe(result => {
              this.router.navigate(['/']);
        });*/
        dialogRef.componentInstance.users = date.user_favorites;
        dialogRef.componentInstance.date = date;
    }

    public chat(date: Date) {
        this.router.navigate(['/chat',date.id,date.user]);
    }
    public confim(date: Date) {
        this.router.navigate(['/confirmed',date.id]);
    }



}
