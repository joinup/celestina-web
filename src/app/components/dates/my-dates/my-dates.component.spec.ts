import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyDatesComponent } from './my-dates.component';

describe('MyDatesComponent', () => {
  let component: MyDatesComponent;
  let fixture: ComponentFixture<MyDatesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyDatesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyDatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
