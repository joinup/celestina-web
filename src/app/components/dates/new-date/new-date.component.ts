import { Component, OnInit, ViewChild, NgZone, ElementRef } from '@angular/core';
import { FormGroup, AbstractControl, FormBuilder, Validators} from '@angular/forms';
import { MdDatepicker, MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { AgmCoreModule, MapsAPILoader } from '@agm/core';

import { InterestService } from '../../../services/interest/interest.service';
import { DateService } from '../../../services/date/date.service';
import { Interest } from '../../../class/interest';

import { DialogComponent } from '../dialog/dialog.component';
@Component({
  selector: 'app-new-date',
  templateUrl: './new-date.component.html',
  styleUrls: ['./new-date.component.scss']
})
export class NewDateComponent implements OnInit {

	public form: FormGroup;
	public name: AbstractControl;
	public date: AbstractControl;
	public time: AbstractControl;
	public location: AbstractControl;
	public duration: AbstractControl;
	public price: AbstractControl;
	public type: AbstractControl;
	public promiseLoading: Promise<any>;
	public types: Array<string>;

    public selectInterests: Array<string>;

    minDate = new Date();

    @ViewChild("search")
    public searchElementRef: ElementRef;

    @ViewChild(MdDatepicker) datepicker: MdDatepicker<Date>;

	constructor(
		fb: FormBuilder,
		public dateService: DateService,
		public interestService: InterestService,
		private snackBar: MdSnackBar,
		private router: Router,
		public dialog: MdDialog,
        private mapsAPILoder: MapsAPILoader,
        private ngZone: NgZone,
	) {
		let pattern = "^\\$?(([1-9](\\d*|\\d{0,2}(,\\d{3})*))|0)(\\.\\d{1,2})?$";
		this.form = fb.group({
        	'date': [null, Validators.required],
	    	'time': [null, Validators.required],
	    	'location': ['',Validators.compose([Validators.required])],
	    	'duration': [''],
	    	'price': ['',Validators.compose([Validators.required, Validators.pattern(pattern)])],
	    	'type': ['',Validators.compose([Validators.required])]
        });

		this.date = this.form.controls['date'];
		this.time = this.form.controls['time'];
		this.location = this.form.controls['location'];
		this.duration = this.form.controls['duration'];
		this.price = this.form.controls['price'];
		this.type = this.form.controls['type'];

	 }

	ngOnInit() {
		this.selectInterests = [];
		this.getTipes();
        this.autocompleteMap();
	}

    public autocompleteMap(){
        this.mapsAPILoder.load().then(() => {
            const autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
                componentRestrictions: {country: "co"}
            });
            autocomplete.addListener("place_changed", () => {
                this.ngZone.run(() => {
                    //get the place result
                    const place: google.maps.places.PlaceResult = autocomplete.getPlace();

                    console.log(place)

                    this.location.setValue(place.formatted_address);


                    //verify result
                    if (place.geometry === undefined || place.geometry === null) {
                        return;
                    }

                    // this.changeLocation = false;
                    // //set latitude, longitude and zoom
                    // this.latitude = place.geometry.location.lat();
                    // this.longitude = place.geometry.location.lng();
                    // this.zoom = 12;
                });
            });
        });

    }

	public getTipes() {
		this.dateService.types().toPromise().then(
			response => {
				this.types = response.json();
			},
			error => {
				console.log(error);
			}

		)
	}

    public changeInterest(event) {
        console.log(event)
        this.selectInterests = event.interest;
    }

	public durationValue() {

		return (this.duration.value && this.duration.value !== 0)? 
			this.duration.value + ' Horas' : 'Illimitado';
	}

	public onSubmit(values) {
        if (this.form.valid) {
            values.interest = this.selectInterests;
            
            if (values.time !== "") {
            	values.time = moment(values.time).format('HH:mm')
            }
            if (values.date !== "") {
            	values.date = moment(values.date).format('Y-MM-DD')
            }
            console.log(values);

            this.promiseLoading = this.dateService.add(values).toPromise().then(
            	response => {
            		this.openSnackBar('Guardado con exito');
                    // setTimeout( () => {
                    //     this.router.navigate(['/']);
                    // },1000);
                    this.openDialog();
            	},
            	error => {
            		this.openSnackBar('¡Ha ocurrido un error! Intente más tarde');

            	}
            );
        }
    }

    public openSnackBar(msg) {
        this.snackBar.open(msg,'', {
          duration: 2000,
        });
    }

    openDialog() {
	    let dialogRef = this.dialog.open(DialogComponent);
	    dialogRef.afterClosed().subscribe(result => {
	      	this.router.navigate(['/']);
	    });
	  }

}

