import { Component, OnInit } from '@angular/core';
import { MdSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { environment } from 'environments/environment';
import { DateService } from '../../services/date/date.service';
import { Interest } from '../../class/interest';
import { Date } from '../../class/date';

@Component({
  selector: 'app-dates',
  templateUrl: './dates.component.html',
  styleUrls: ['./dates.component.scss']
})
export class DatesComponent implements OnInit {

    public url = environment.url;
    public avatar = "./assets/imagenes/m3.jpg"
    public favorite = "favorite_border";
    public rating = 4;

    public loading = true;

    public promiseLoading: Promise<any>;

    public dates: Date[];


    constructor(public dateService: DateService, private snackBar: MdSnackBar,public router: Router) { }

    ngOnInit() {
        this.getDates();
    }

    public getDates() {
        this.dateService.get().toPromise().then(
            response => {
                this.dates = response.json();
                this.loading = false;
            },
            error => {
                console.log(error)
            }

        )
    }

    public getAvatar(date) {
        return date.avatar? this.url + date.avatar: this.avatar;
    }

    public setfavorite(date: Date) {
        console.log(date)
        this.promiseLoading = (+date.favorite)?
            this.dateService.removeFavorite(date.id).toPromise()
        :
            this.dateService.addFavorite(date.id).toPromise();

        this.promiseLoading.then(
            response => {
                this.openSnackBar(response.json().msg)
                date.favorite = !date.favorite;
                if (date.favorite) {
                    this.router.navigate(['/chat',date.id,date.user]);
                }
            },
            error => {
                this.openSnackBar(error.json().msg)
            }
        )
    }

    public round(num: number){
        return Math.round(num);
    }

    public openSnackBar(msg) {
        this.snackBar.open(msg,'', {
          duration: 2000,
        });
    }

}
