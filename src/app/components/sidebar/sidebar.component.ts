import { Component, OnInit, HostListener } from '@angular/core';
import { LoginService } from '../../services/login/login.service';
import { environment } from 'environments/environment';
import { ProfileService } from '../../services/profile/profile.service';
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
    public mode: string = 'side';
    public avatar: string;
    public url = environment.url;
    public name: string;
    @HostListener('window:resize',['$event'])
    onResize(event) {
        let width = event.target.innerWidth;
        this.resize(width);
    }


    constructor(private loginService: LoginService, public profileService: ProfileService) { }

    ngOnInit() {
        let width = window.screen.width;
        this.resize(width);
        this.avatar = './assets/avatar.png';
        this.getProfile();
    }

    public getProfile() {
        this.profileService.get().toPromise().then(
            response => {
                localStorage.setItem('profile', JSON.stringify(response.json()));
                this.name = response.json().nickname;
                if (response.json().avatar) {
                    this.avatar = this.url + response.json().avatar;
                }
            },
            error => {
                console.log(error);
            }
        );
    }

    public resize(width) {
      this.mode = (width < 550)? 'over': 'side';
    }

    public logout() {
        this.loginService.logout();
    }

}
