import { Component, OnInit } from '@angular/core';
import { FormGroup, AbstractControl, FormBuilder, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { MdSnackBar } from '@angular/material';

import { ProfileService } from '../../services/profile/profile.service';

import { InterestService } from '../../services/interest/interest.service';
import { Interest } from '../../class/interest';
import { User } from '../../class/user';


@Component({
  selector: 'app-config',
  templateUrl: './config.component.html',
  styleUrls: ['./config.component.scss']
})
export class ConfigComponent implements OnInit {

    public interest: Interest;
    public interests: Interest[];
    public displayInterests: Interest[];
    public selectInterests: Interest[];
    public form: FormGroup;
    public tdc: AbstractControl;
    public paypal: AbstractControl;
    public bio: AbstractControl;
    public promiseLoading: Promise<any>;
    public profile: User;

    constructor(
        public interestService: InterestService,
        fb: FormBuilder,
        public profileService: ProfileService,
        private router: Router,
        private snackBar: MdSnackBar,
    ) {
        this.form = fb.group({
          'tdc': ['', ],
          'paypal': ['', ],
          'bio': ['', ],
        });

        this.tdc = this.form.controls['tdc'];
        this.paypal = this.form.controls['paypal'];
        this.bio = this.form.controls['bio'];
    }

    ngOnInit() {
        this.selectInterests = [];
        this.getProfile();
    }

    public getProfile() {
        this.profileService.get().toPromise().then(
            response => {
                this.profile = response.json();
                localStorage.setItem('profile', JSON.stringify(response.json()));
                let oldInterests = response.json().interest;
                if (oldInterests.length) {
                    for (var i = oldInterests.length - 1; i >= 0; i--) {
                        this.selectInterests.push(oldInterests[i].id);
                    }
                }
                console.log(oldInterests)
                this.tdc.setValue(this.profile.tdc);
                this.paypal.setValue(this.profile.paypal);
                this.bio.setValue(this.profile.bio);
            },
            error => {
                console.log(error);
            }
        );
    }

    public changeInterest(event) {
        console.log(event)
        this.selectInterests = event.interest;
    }

    // public getInterests() {
    //     this.interestService.get().toPromise().then(
    //         response => {
    //             this.interests = response.json();
    //         },
    //         error => {
    //             console.log(error);

    //         }

    //     );
    // }

    // public addInterest(interest: Interest){
    //     if (this.selectInterests.indexOf(interest) === -1) {
    //         this.selectInterests.push(interest);
    //     }
    // }

    // public deleteInterest(interest: Interest){
    //     let index = this.selectInterests.indexOf(interest);
    //     if (index !== -1) {
    //         this.selectInterests.splice(index, 1);
    //     }
    // }    

    // public filterInterest(name) {
    //     this.displayInterests = [];
    //     let end = (this.interests.length > 9)? 9: this.interests.length;
    //     for (let i = this.interests.length - 1; i >= 0; i--) {
    //         let value = this.interests[i].name;
    //         if (!name || value.toLowerCase().indexOf(name.toLowerCase()) > -1 ) {
    //             this.displayInterests.push(this.interests[i]);
    //         }
    //         if (this.displayInterests.length == 9) {
    //             i = -1;
    //         }
    //     }
    // }

    public onSubmit(values) {
        if (this.form.valid) {
            values.interest = [];
            // for (var i = this.selectInterests.length - 1; i >= 0; i--) {
            //     values.interest.push(this.selectInterests[i].id);
            // }
            values.interest = this.selectInterests;
            this.promiseLoading = this.profileService.update(values).toPromise().then(
                response => {
                    this.openSnackBar('¡Guardado exitosamente!');
                    if (this.profile.activated == 1) {
                        this.router.navigate(['/novalidate']);
                    }else{
                        this.router.navigate(['/']);

                    }
                },
                error => {
                    this.openSnackBar('¡Ha ocurrido un error! Intente más tarde');

                }
            );
        }
    }

    public openSnackBar(msg) {
        this.snackBar.open(msg,'', {
          duration: 2000,
        });
    }

}
