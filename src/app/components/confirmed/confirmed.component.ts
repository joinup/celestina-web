import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';
import { ProfileService } from '../../services/profile/profile.service';
import { DateService } from '../../services/date/date.service';

import { User } from '../../class/user';
import { Date } from '../../class/date';
@Component({
  selector: 'app-confirmed',
  templateUrl: './confirmed.component.html',
  styleUrls: ['./confirmed.component.scss']
})
export class ConfirmedComponent implements OnInit {

	public id_date: number;
	public profile: User;
	public user: User;
	public date: Date;

	public diff_time: number;
	public pass: number;



  	constructor(
  		public profileService: ProfileService,
  		public dateService: DateService,
  		private route: ActivatedRoute,
  		private router: Router,

  	) { }

	ngOnInit() {
		this.route.params.subscribe(params => {
           this.id_date = params['date'];
           
        });
		this.getProfile();
	}

	public getProfile() {
	    this.profileService.get().toPromise().then(
	        response => {
	            this.profile = response.json();
	            localStorage.setItem('profile', JSON.stringify(response.json()));
	            this.getDate(this.id_date);
	        },
	        error => {
	            console.log(error);
	        }
	    );
	}

	public getDate(date: number) {
	    this.dateService.find(date).toPromise().then(
	        response => {
	            this.date = response.json();
	           	if (response.json().user_info.id === this.profile.id ) {
	           		this.user = response.json().with_user;
	           	} else {
	           		this.user = response.json().user_info;
	           	}
	           	this.setDiffTime();
	        },
	        error => {
	            console.log(error);
	        }
	    );
	}

	public setDiffTime() {
       	let today = moment();
       	let day = moment(this.date.date + ' ' + this.date.time, 'YYYY-MM-DD HH:mm' );

       	this.diff_time = day.diff(today, 'minutes');
       	console.log(this.diff_time);
       	console.log(this.date.date + ' ' + this.date.time);
       	console.log(today.format('YYYY-MM-DD hh:mm a'));
       	console.log(day.format('YYYY-MM-DD hh:mm a'));

       	if (this.diff_time > 30) {
       		this.pass = 1;
       	} else if(this.diff_time < 1 ){
       		this.router.navigate(['/rate',this.date.id]);
       	} else {
       		this.pass = 2;
       	}
		
	}



}
