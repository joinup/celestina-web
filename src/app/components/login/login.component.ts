import { Component, OnInit } from '@angular/core';
import { FormGroup, AbstractControl, FormBuilder, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { MdSnackBar } from '@angular/material';
import { FacebookService, InitParams, LoginResponse } from 'ng2-facebook-sdk';

import { LoginService } from '../../services/login/login.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    public form: FormGroup;
    public email: AbstractControl;
    public password: AbstractControl;
    public promiseLoading: Promise<any>;

    constructor(
          formB: FormBuilder,
          private fb: FacebookService,
          private loginService: LoginService,
          private router: Router,
          private snackBar: MdSnackBar,
    ) {

        this.form = formB.group({
          'email': ['', Validators.compose([Validators.required, Validators.minLength(4), Validators.email])],
          'password': ['', Validators.compose([Validators.required, Validators.minLength(4)])]
        });

        this.email = this.form.controls['email'];
        this.password = this.form.controls['password'];

        let initParams: InitParams = {
          appId: '731528770363855',
          cookie: true,
          xfbml: true,
          version: 'v2.8'
        };
     
        this.fb.init(initParams);

    }

    ngOnInit() {}

    public logued(token){
      this.openSnackBar('Bienvenido');
      localStorage.setItem('id_token', token);
      this.loginService.refreshToken();
      this.router.navigate(['/']);
    }

    public onSubmit(values: Object): void {
        if (this.form.valid) {
            this.promiseLoading = this.loginService.login(values).toPromise().then(
                response => {
                    this.logued(response.json().token);
                },
                error => {
                    if ( error.status === 401 ) {
                      this.openSnackBar('Datos incorrectos');
                    }
                    if ( error.status === 500 ) {
                        this.openSnackBar('¡Ha ocurrido un error! Intente más tarde');
                    }
                }
            );
        } else {
              this.openSnackBar('Complete los campos');
        }
    }

    loginWithFacebook(): void {
        this.fb.login()
            .then((response: LoginResponse) => {
                console.log(response)
                if (response.status === 'connected') {
                    this.promiseLoading = this.loginService.loginFacebook({code: response.authResponse.accessToken}).toPromise().then(
                        response => {
                            this.logued(response.json().token);
                        },
                        error => {
                            if ( error.status === 401 ) {
                              this.openSnackBar(error.json().error);
                            }
                            if ( error.status === 500 ) {
                                this.openSnackBar('¡Ha ocurrido un error! Intente más tarde');
                            }
                        },
                    );
                }
            })
            .catch((error: any) => console.error(error));
   
    }

    public openSnackBar(msg) {
        this.snackBar.open(msg,'', {
            duration: 2000,
        });
    }

}
