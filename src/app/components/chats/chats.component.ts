import { Component, OnInit } from '@angular/core';
import { environment } from 'environments/environment';
import { ChatService } from 'app/services/chat/chat.service';
import { ProfileService } from 'app/services/profile/profile.service';
import { User } from '../../class/user';
import { Date } from '../../class/date';
import { Chat } from '../../class/chat';

@Component({
  selector: 'app-chats',
  templateUrl: './chats.component.html',
  styleUrls: ['./chats.component.scss']
})
export class ChatsComponent implements OnInit {

	public url = environment.url;
	public chats: Array<any>;
	public prewiewAvatar = './assets/avatar.png';
	public profile: User;

  	constructor(public chatService: ChatService, public profileService: ProfileService) { }

  	ngOnInit() {
  		this.getProfile();
  	}
  	public getProfile() {
	    this.profileService.get().toPromise().then(
	        response => {
	            this.profile = response.json();
	            localStorage.setItem('profile', JSON.stringify(response.json()));
		  		this.getChats();
	        },
	        error => {
	            console.log(error);
	        }
	    );
	}

  	public getChats() {
        this.chatService.all().toPromise().then(
            response => {
                this.chats = response.json();
                this.chats.forEach((value, i)=>{
                	if (value.info.user_send.id === this.profile.id ) {
		           		value.info.user = value.info.user_receiver;
		           	} else {
		           		value.info.user = value.info.user_send;
		           	}
                });
                console.log(this.chats)
            },
            error => {
                console.log(error)
            }
        )
    }

    public getAvatar(user): string{
        if (user) {
            return this.url + user.avatar;
        } else {
            return this.prewiewAvatar;
        }
    }

}
