import { Component, OnInit } from '@angular/core';
import { FormGroup, AbstractControl, FormBuilder, Validators} from '@angular/forms';
import { MdSnackBar } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { DateService } from '../../../services/date/date.service';
@Component({
  selector: 'app-dialog-change-price',
  templateUrl: './dialog-change-price.component.html',
  styleUrls: ['./dialog-change-price.component.scss']
})
export class DialogChangePriceComponent implements OnInit {

	public id_date: number
	public form: FormGroup;
    public price: AbstractControl;
    public promiseLoading: Promise<any>;


	constructor(
		fb: FormBuilder,
		public dateService: DateService,
		private route: ActivatedRoute,
		private snackBar: MdSnackBar,
	) {
		this.form = fb.group({
		  'price': ['',Validators.compose([Validators.required])],
		});

		this.price = this.form.controls['price'];

		this.route.params.subscribe(params => {
           this.id_date = params['date'];
        });
	}

	ngOnInit() {}

	public save(value: Object){
		if (this.form.valid) {
            this.promiseLoading = this.dateService.editPrice(this.id_date, value).toPromise().then(
                response => {
                    this.openSnackBar('¡Guardado exitosamente!');
                },
                error => {

                    if ( error.status === 400 ) {
                      this.openSnackBar(error.json().msg);
                    }
                    if ( error.status === 500 ) {
                        this.openSnackBar('¡Ha ocurrido un error! Intente más tarde');
                    }

                }
            );
        }
    }

    public openSnackBar(msg) {
        this.snackBar.open(msg,'', {
          duration: 2000,
        });
    }

}
