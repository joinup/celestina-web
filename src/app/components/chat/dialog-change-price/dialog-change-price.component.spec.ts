import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogChangePriceComponent } from './dialog-change-price.component';

describe('DialogChangePriceComponent', () => {
  let component: DialogChangePriceComponent;
  let fixture: ComponentFixture<DialogChangePriceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogChangePriceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogChangePriceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
