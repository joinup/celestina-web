import { Component, OnInit, OnDestroy, ElementRef, ViewChild } from '@angular/core';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, AbstractControl, FormBuilder, Validators} from '@angular/forms';
import * as moment from 'moment';
import { environment } from 'environments/environment';
import { ChatService } from 'app/services/chat/chat.service';
import { User } from '../../class/user';
import { Date } from '../../class/date';
import { Chat } from '../../class/chat';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit {

    public url = environment.url;
    public id_date: string;
    public id_user: string;
    private sub: any;
    public prewiewAvatar = './assets/avatar.png';

    public profile: User;
    public user: User;
    public date: Date;
    public chats: Array<Chat>;

    public form: FormGroup;
    public text: AbstractControl;

    private repeat: boolean;

    @ViewChild('chatBox') private chatBox: ElementRef;

    @ViewChild('container') private container: ElementRef;

    constructor(
        private chatService: ChatService,
        private route: ActivatedRoute,
        public dialog: MdDialog,
        fb: FormBuilder,
    ) {
        this.form = fb.group({
          'text': ['',Validators.compose([Validators.required])],
        });

        this.text = this.form.controls['text'];
    }

    ngOnInit() {
        this.repeat = true;
        this.sub = this.route.params.subscribe(params => {
           this.id_date = params['date'];
           this.id_user = params['user'];
           this.getChats(this.id_date, this.id_user);
        });
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
        this.repeat = false;
    }

    public getChats(date, user) {
        this.chatService.get(date, user).toPromise().then(
            response => {
                let resp = response.json();
                this.user = resp.user;
                this.profile = resp.profile;
                this.date = resp.date;
                this.chats = resp.chat;
                this.newMenssages();
            },
            error => {
                console.log(error)
            }
        )
    }

    public getAvatar(user: User): string{
        if (user) {
            return this.url + user.avatar;
        } else {
            return this.prewiewAvatar;
        }
    }

    public sent(values: any) {
        let data = {
            text: values.text,
            date: this.id_date,
            user: this.id_user,
        }
        this.form.reset();
        let chat: Chat = {
            date: +this.id_date,
            created_at: moment().format('YYYY-MM-DD HH:mm'),
            text: values.text,
            user_receiver: +this.id_user,
            user_send: this.profile.id,
        }

        this.chats.push(chat);

        this.scrollDown();

        this.chatService.sent(data).toPromise().then(
            response => {

                console.log(response)
            },
            error => {

            }
        )
    }

    public newMenssages(){
        this.read();
        //setInterval(()=>this.read(),5000);
    }

    public read() {
        this.chatService.read(this.id_date, this.id_user).toPromise().then(
            response => {
                let resp: Chat[] = response.json();
                console.log(resp)
                if (resp.length) {
                    resp.forEach((value, i)=>{
                        this.chats.push(value);
                    })
                    this.scrollDown();
                }
                if (this.repeat) {
                    setTimeout(() => this.read(),2000);
                }

            },
            error => {

            }
        );
    } 

    public scrollDown() {
        setTimeout(() => {
            //this.chatBox.nativeElement.scrollTop = this.chatBox.nativeElement.scrollHeight;
            this.container.nativeElement.scrollTop = this.container.nativeElement.scrollHeight;
            console.log(this.container.nativeElement.scrollHeight)
        },100);
    }

}
